package com.getjavajob.training.algo.init.cherkashinA;

/**
 * Created by Александр on 05.04.2017.
 */

import static com.getjavajob.training.algo.init.cherkashinA.TaskCh02N043.isDivided;
import static com.getjavajob.training.algo.util.Assert.assertEquals;

public class TaskCh02N043Test {
    public static void main(String[] args) {
        testDivided();
        testUndivided();
    }

    public static void testDivided() {
        assertEquals("TaskCh02N043Test.testDivided", 1, isDivided(2, 4));
    }

    public static void testUndivided() {
        assertEquals("TaskCh02N043Test.testUndivided", "No 1", isDivided(3, 4));
    }

}