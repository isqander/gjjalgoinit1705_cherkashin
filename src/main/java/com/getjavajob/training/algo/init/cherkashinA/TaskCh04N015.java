/**
 * Created by Александр on 13.03.2017.
 */

package com.getjavajob.training.algo.init.cherkashinA;

public class TaskCh04N015 {

    public static int calculateAge(int birthYear, int birthMonth, int todayYear, int todayMonth) {
        int allMonth = (todayYear - birthYear) * 12 + todayMonth - birthMonth;
        return allMonth / 12;
    }

}
