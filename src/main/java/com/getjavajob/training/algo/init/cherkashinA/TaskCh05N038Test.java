package com.getjavajob.training.algo.init.cherkashinA;

/**
 * Created by Александр on 07.04.2017.
 */

import static com.getjavajob.training.algo.init.cherkashinA.TaskCh05N038.findPosition;
import static com.getjavajob.training.algo.init.cherkashinA.TaskCh05N038.findTrack;
import static com.getjavajob.training.algo.util.Assert.assertEquals;

public class TaskCh05N038Test {

    public static void main(String[] args) {
        testFindPositionA();
        testFindTrackB();
    }

    public static void testFindPositionA() {
        assertEquals("TaskCh05N038Test.testFindPositionA", 1.0 - 1.0 / 2.0, findPosition(2));
    }

    public static void testFindTrackB() {
        assertEquals("TaskCh05N038Test.testFindTrackB", 1.0 + 1.0 / 2.0, findTrack(2));
    }

}
