/**
 * Created by Александр on 17.03.2017.
 */

package com.getjavajob.training.algo.init.cherkashinA;

public class TaskCh10N051 {

    static void procedure1(int n) {
        if (n > 0) {
            System.out.print(n);
            procedure1(n - 1);
        }
    }

    static void procedure2(int n) {
        if (n > 0) {
            procedure2(n - 1);
            System.out.print(n);
        }
    }

    static void procedure3(int n) {
        if (n > 0) {
            System.out.print(n);
            procedure3(n - 1);
            System.out.print(n);
        }
    }

    public static void main(String[] args) {
        int n = 5;
        procedure1(n);
        System.out.println();
        procedure2(n);
        System.out.println();
        procedure3(n);
    }

}
