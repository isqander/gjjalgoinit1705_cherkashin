package com.getjavajob.training.algo.init.cherkashinA;

/**
 * Created by Александр on 01.05.2017.
 */

import static com.getjavajob.training.algo.util.Assert.assertEquals;

public class TaskCh06N087Test {

    public static void main(String[] args) {
        //Game game = new Game();
        testScore();
        testResult();
    }

    public static void testScore() {
        Game game = new Game("Roza", "Poza");
        game.team1score = 3;
        game.team2score = 4;
        assertEquals("TaskCh06N087Test.testScore", "Team Roza score is 3" + System.lineSeparator() + "Team Poza score is 4" + System.lineSeparator(), game.score());
    }

    public static void testResult() {
        Game game = new Game("Roza", "Poza");
        game.team1score = 3;
        game.team2score = 4;
        assertEquals("TaskCh06N087Test.testResult", "The winner is Poza. It's result is 4" + System.lineSeparator() + "Roza is looser. It's result score is 3" + System.lineSeparator(), game.result());
    }

}
