/**
 * Created by Александр on 12.03.2017.
 */

package com.getjavajob.training.algo.init.cherkashinA;

import java.util.Scanner;

public class TaskCh02N031 {

    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        while (true){
            int n = scan.nextInt();
            if (n <= 999 && n >= 100){
                System.out.println(findNumber(n));
                break;
            } else System.out.println("Wrong number input must be between 100 and 999");
        }

    }

    public static int findNumber(int n) {
        int firstFigure = n / 100;
        int twoFigures = n % 100;
        int secondFigure = twoFigures / 10;
        int thirdFigure = twoFigures % 10;
        return firstFigure * 100 + thirdFigure * 10 + secondFigure;
    }

}
