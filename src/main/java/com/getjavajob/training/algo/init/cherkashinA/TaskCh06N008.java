/**
 * Created by Александр on 14.03.2017.
 */

package com.getjavajob.training.algo.init.cherkashinA;

import java.util.Scanner;

public class TaskCh06N008 {

    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        System.out.println(notBiggerSquare(Integer.parseInt(scan.nextLine())));
    }

    public static int notBiggerSquare(int n) {
        int[] ar = new int[n];
        for (int i = 0; i < ar.length; i++) {
            ar[i] = (i + 1) * (i + 1);
            if (ar[i] > n) {
                return ar[i - 1];
            }
        }
        return 0;
    }

}
