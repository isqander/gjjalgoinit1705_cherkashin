/**
 * Created by Александр on 15.03.2017.
 */

package com.getjavajob.training.algo.init.cherkashinA;

public class TaskCh09N166 {

    public static String changeWords(String sentence) {
        String[] sentenceArray = sentence.split("[ .]");
        StringBuilder answer = new StringBuilder(sentenceArray[sentenceArray.length - 1] + " ");
        for (int i = 1; i < sentenceArray.length - 1; i++) {
            answer.append(sentenceArray[i]).append(' ');
        }
        answer.append(sentenceArray[0]).append('.');
        return answer.toString();
    }

}
