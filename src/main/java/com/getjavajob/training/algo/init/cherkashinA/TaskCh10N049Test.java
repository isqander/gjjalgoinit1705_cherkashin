package com.getjavajob.training.algo.init.cherkashinA;

/**
 * Created by Александр on 11.04.2017.
 */

import static com.getjavajob.training.algo.init.cherkashinA.TaskCh10N049.maxIndex;
import static com.getjavajob.training.algo.util.Assert.assertEquals;

public class TaskCh10N049Test {

    public static void main(String[] args) {
        testMaxIndex();
    }

    public static void testMaxIndex() {
        int[] arr = {4, 8, 3, 9, 1};
        assertEquals("TaskCh10N049Test.testMaxIndex", 3, maxIndex(arr));
    }

}
