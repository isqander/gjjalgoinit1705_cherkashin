/**
 * Created by Александр on 14.03.2017.
 */

package com.getjavajob.training.algo.init.cherkashinA;

import java.util.Scanner;

public class TaskCh06N087 {

    public static void main(String[] args) {
        Game game = new Game();
        game.play();
        System.out.println(game.result());
    }

}

class Game {
    //What is wrong about static variables:
    //If I create more some Game instances and change a static variable in one object it'll also be changed in other objects
    String team1name;
    String team2name;
    int team1score;
    int team2score;
    Scanner sc = new Scanner(System.in);
    String separator = System.lineSeparator();

    public Game(String team1name, String team2name) {
        this.team1name = team1name;
        this.team2name = team2name;
    }

    public Game() {
        System.out.println("Enter team #1");
        team1name = sc.nextLine();
        System.out.println("Enter team #2");
        team2name = sc.nextLine();
    }

    public String score() {
        return "Team " + team1name + " score is " + team1score + separator + "Team " + team2name + " score is " + team2score + separator;
    }

    public String result() {
        if (team1score == team2score) {
            return "There is no winners. Teams have the same score.";
        } else if (team1score > team2score) {
            return "The winner is " + team1name + ". It's result is " + team1score
                    + separator + team2name + " is looser. It's result score is " + team2score + separator;
        } else {
            return "The winner is " + team2name + ". It's result is " + team2score
                    + separator + team1name + " is looser. It's result score is " + team1score + separator;
        }
    }

    void play() {
        while (true) {
            System.out.println("Enter team to score (1 or 2 or 0 to finish game):");
            int teamToScore = sc.nextInt();
            switch (teamToScore) {
                case 0:
                    return;
                case 1: {
                    team1score += this.enterScore();
                    break;
                }
                case 2: {
                    team2score += this.enterScore();
                    break;
                }
                default:
                    System.out.println("Wrong number");
            }
            System.out.println(score());
        }
    }

    int enterScore() {
        while (true) {
            System.out.println("Enter score (1 or 2 or 3):");
            int addScore = sc.nextInt();
            if (addScore > 0 && addScore < 4) {
                return addScore;
            } else {
                System.out.println("Wrong number");
            }
        }

    }

}