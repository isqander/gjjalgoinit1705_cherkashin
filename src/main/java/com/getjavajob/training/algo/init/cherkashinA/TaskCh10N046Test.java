package com.getjavajob.training.algo.init.cherkashinA;

/**
 * Created by Александр on 11.04.2017.
 */

import static com.getjavajob.training.algo.init.cherkashinA.TaskCh10N046.findNmember;
import static com.getjavajob.training.algo.init.cherkashinA.TaskCh10N046.sumNmembers;
import static com.getjavajob.training.algo.util.Assert.assertEquals;

public class TaskCh10N046Test {

    public static void main(String[] args) {
        testFindNmember();
        testSumNmembers();
    }

    public static void testFindNmember() {
        assertEquals("TaskCh10N046Test.testFindNmember", 8, findNmember(2, 2, 3));
    }

    public static void testSumNmembers() {
        assertEquals("TaskCh10N046Test.testSumNmembers", 14, sumNmembers(2, 2, 3));
    }

}