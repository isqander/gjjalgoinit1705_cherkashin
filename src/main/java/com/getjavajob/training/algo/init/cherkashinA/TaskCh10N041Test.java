package com.getjavajob.training.algo.init.cherkashinA;

/**
 * Created by Александр on 09.04.2017.
 */

import static com.getjavajob.training.algo.init.cherkashinA.TaskCh10N041.factorial;
import static com.getjavajob.training.algo.util.Assert.assertEquals;

public class TaskCh10N041Test {

    public static void main(String[] args) {
        testFactorial();
    }

    public static void testFactorial() {
        assertEquals("TaskCh10N041Test.testFactorial", 24, factorial(4));
    }

}

