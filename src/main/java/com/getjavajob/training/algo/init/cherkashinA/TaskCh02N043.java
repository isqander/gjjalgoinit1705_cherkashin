/**
 * Created by Александр on 12.03.2017.
 */

package com.getjavajob.training.algo.init.cherkashinA;

public class TaskCh02N043 {

    public static int isDivided(int a, int b) {
        return a % b * (b % a) + 1;
    }

}
