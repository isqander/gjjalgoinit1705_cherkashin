/**
 * Created by Александр on 17.03.2017.
 */

package com.getjavajob.training.algo.init.cherkashinA;

import java.util.Arrays;

import static com.getjavajob.training.algo.init.cherkashinA.TaskCh10N048.maxElement;

public class TaskCh10N049 {

    static int maxIndex(int[] a) {
        if (maxElement(a) == a[a.length - 1]) {
            return a.length - 1;
        }
        int[] b = Arrays.copyOf(a, a.length - 1);
        return maxIndex(b);
    }

}
