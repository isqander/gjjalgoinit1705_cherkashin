package com.getjavajob.training.algo.init.cherkashinA;

/**
 * Created by Александр on 09.04.2017.
 */

import static com.getjavajob.training.algo.init.cherkashinA.TaskCh09N166.changeWords;
import static com.getjavajob.training.algo.util.Assert.assertEquals;

public class TaskCh09N166Test {

    public static void main(String[] args) {
        testChangeWords();
    }

    public static void testChangeWords() {
        assertEquals("TaskCh09N166Test.testChangeWords", "zero finished with exit code Process.", changeWords("Process finished with exit code zero."));
    }

}

