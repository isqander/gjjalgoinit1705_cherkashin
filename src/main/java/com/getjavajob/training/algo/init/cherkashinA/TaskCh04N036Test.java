package com.getjavajob.training.algo.init.cherkashinA;

/**
 * Created by Александр on 07.04.2017.
 */

import static com.getjavajob.training.algo.init.cherkashinA.TaskCh04N036.showTrafficLight;
import static com.getjavajob.training.algo.util.Assert.assertEquals;

public class TaskCh04N036Test {

    public static void main(String[] args) {
        testShowTrafficLight1();
        testShowTrafficLight2();
    }

    public static void testShowTrafficLight1() {
        assertEquals("TaskCh04N036Test.testShowTrafficLight1", "red", showTrafficLight(3));
    }

    public static void testShowTrafficLight2() {
        assertEquals("TaskCh04N036Test.testShowTrafficLight2", "green", showTrafficLight(5));
    }

}
