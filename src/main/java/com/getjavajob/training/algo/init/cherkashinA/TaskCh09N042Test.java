package com.getjavajob.training.algo.init.cherkashinA;

/**
 * Created by Александр on 09.04.2017.
 */

import static com.getjavajob.training.algo.init.cherkashinA.TaskCh09N042.reverse;
import static com.getjavajob.training.algo.util.Assert.assertEquals;

public class TaskCh09N042Test {

    public static void main(String[] args) {
        testReverse();
    }

    public static void testReverse() {
        assertEquals("TaskCh09N042Test.testReverse", "cba", reverse("abc"));
    }

}
