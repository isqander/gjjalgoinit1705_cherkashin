package com.getjavajob.training.algo.init.cherkashinA;

/**
 * Created by Александр on 17.04.2017.
 */

import static com.getjavajob.training.algo.init.cherkashinA.TaskCh12N023.printArray;

public class TaskCh12N025 {

    public static void main(String[] args) {
        System.out.println("а");
        printArray(makeArrayA());
        System.out.println("б");
        printArray(makeArrayB());
        System.out.println("в");
        printArray(makeArrayC());
        System.out.println("г");
        printArray(makeArrayD());
        System.out.println("д");
        printArray(makeArrayE());
        System.out.println("е");
        printArray(makeArrayF());
        System.out.println("ж");
        printArray(makeArrayG());
        System.out.println("з");
        printArray(makeArrayH());
        System.out.println("и");
        printArray(makeArrayI());
        System.out.println("к");
        printArray(makeArrayK());
        System.out.println("л");
        printArray(makeArrayL());
        System.out.println("м");
        printArray(makeArrayM());
        System.out.println("н");
        printArray(makeArrayN());
        System.out.println("о");
        printArray(makeArrayO());
        System.out.println("п");
        printArray(makeArrayP());
        System.out.println("р");
        printArray(makeArrayQ());
    }

    public static int[][] makeArrayA() {   //а
        int[][] arr = new int[12][10];
        int paste = 1;
        for (int i = 0; i < arr.length; i++) {
            for (int j = 0; j < arr[0].length; j++) {
                arr[i][j] = paste++;
            }
        }
        return arr;
    }

    public static int[][] makeArrayB() {   //б
        int[][] arr = new int[12][10];
        int paste = 1;
        for (int j = 0; j < arr[0].length; j++) {
            for (int i = 0; i < arr.length; i++) {
                arr[i][j] = paste++;
            }
        }
        return arr;
    }

    public static int[][] makeArrayC() {   //в
        int[][] arr = new int[12][10];
        int paste = 1;
        for (int i = 0; i < arr.length; i++) {
            for (int j = arr[0].length - 1; j >= 0; j--) {
                arr[i][j] = paste++;
            }
        }
        return arr;
    }

    public static int[][] makeArrayD() {   //г
        int[][] arr = new int[12][10];
        int paste = 1;
        for (int j = 0; j < arr[0].length; j++) {
            for (int i = arr.length - 1; i >= 0; i--) {
                arr[i][j] = paste++;
            }
        }
        return arr;
    }

    public static int[][] makeArrayE() {   //д
        int[][] arr = new int[10][12];
        int paste = 1;
        for (int i = 0; i < arr.length; i++) {
            if (i % 2 == 0) {
                for (int j = 0; j < arr[0].length; j++) {
                    arr[i][j] = paste++;
                }
            } else {
                for (int j = arr[0].length - 1; j >= 0; j--) {
                    arr[i][j] = paste++;
                }
            }
        }
        return arr;
    }

    public static int[][] makeArrayF() {   //е
        int[][] arr = new int[12][10];
        int paste = 1;
        for (int j = 0; j < arr[0].length; j++) {
            if (j % 2 == 0) {
                for (int i = 0; i < arr.length; i++) {
                    arr[i][j] = paste++;
                }
            } else {
                for (int i = arr.length - 1; i >= 0; i--) {
                    arr[i][j] = paste++;
                }
            }
        }
        return arr;
    }

    public static int[][] makeArrayG() {   //ж
        int[][] arr = new int[12][10];
        int paste = 1;
        for (int i = arr.length - 1; i >= 0; i--) {
            for (int j = 0; j < arr[0].length; j++) {
                arr[i][j] = paste++;
            }
        }
        return arr;
    }

    public static int[][] makeArrayH() {   //з
        int[][] arr = new int[12][10];
        int paste = 1;
        for (int j = arr[0].length - 1; j >= 0; j--) {
            for (int i = 0; i < arr.length; i++) {
                arr[i][j] = paste++;
            }
        }
        return arr;
    }


    public static int[][] makeArrayI() {   //и
        int[][] arr = new int[12][10];
        int paste = 1;
        for (int i = arr.length - 1; i >= 0; i--) {
            for (int j = arr[0].length - 1; j >= 0; j--) {
                arr[i][j] = paste++;
            }
        }
        return arr;
    }

    public static int[][] makeArrayK() {   //к
        int[][] arr = new int[12][10];
        int paste = 1;
        for (int j = arr[0].length - 1; j >= 0; j--) {
            for (int i = arr.length - 1; i >= 0; i--) {
                arr[i][j] = paste++;
            }
        }
        return arr;
    }

    public static int[][] makeArrayL() {   //л
        int[][] arr = new int[12][10];
        int paste = 1;
        for (int i = arr.length - 1; i >= 0; i--) {
            if (i % 2 != 0) {
                for (int j = 0; j < arr[0].length; j++) {
                    arr[i][j] = paste++;
                }
            } else {
                for (int j = arr[0].length - 1; j >= 0; j--) {
                    arr[i][j] = paste++;
                }
            }
        }
        return arr;
    }

    public static int[][] makeArrayM() {   //м
        int[][] arr = new int[12][10];
        int paste = 1;
        for (int i = 0; i < arr.length; i++) {
            if (i % 2 != 0) {
                for (int j = 0; j < arr[0].length; j++) {
                    arr[i][j] = paste++;
                }
            } else {
                for (int j = arr[0].length - 1; j >= 0; j--) {
                    arr[i][j] = paste++;
                }
            }
        }
        return arr;
    }

    public static int[][] makeArrayN() {   //н
        int[][] arr = new int[12][10];
        int paste = 1;
        for (int j = arr[0].length - 1; j >= 0; j--) {
            if (j % 2 != 0) {
                for (int i = 0; i < arr.length; i++) {
                    arr[i][j] = paste++;
                }
            } else {
                for (int i = arr.length - 1; i >= 0; i--) {
                    arr[i][j] = paste++;
                }
            }
        }
        return arr;
    }

    public static int[][] makeArrayO() {   //о
        int[][] arr = new int[12][10];
        int paste = 1;
        for (int j = 0; j < arr[0].length; j++) {
            if (j % 2 != 0) {
                for (int i = 0; i < arr.length; i++) {
                    arr[i][j] = paste++;
                }
            } else {
                for (int i = arr.length - 1; i >= 0; i--) {
                    arr[i][j] = paste++;
                }
            }
        }
        return arr;
    }

    public static int[][] makeArrayP() {   //п
        int[][] arr = new int[12][10];
        int paste = 1;
        for (int i = arr.length - 1; i >= 0; i--) {
            if (i % 2 == 0) {
                for (int j = 0; j < arr[0].length; j++) {
                    arr[i][j] = paste++;
                }
            } else {
                for (int j = arr[0].length - 1; j >= 0; j--) {
                    arr[i][j] = paste++;
                }
            }
        }
        return arr;
    }

    public static int[][] makeArrayQ() {   //р
        int[][] arr = new int[12][10];
        int paste = 1;
        for (int j = arr[0].length - 1; j >= 0; j--) {
            if (j % 2 == 0) {
                for (int i = 0; i < arr.length; i++) {
                    arr[i][j] = paste++;
                }
            } else {
                for (int i = arr.length - 1; i >= 0; i--) {
                    arr[i][j] = paste++;
                }
            }
        }
        return arr;
    }
}
