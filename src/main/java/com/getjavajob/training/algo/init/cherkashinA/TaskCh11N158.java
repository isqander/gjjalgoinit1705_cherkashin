/**
 * Created by Александр on 18.03.2017.
 */

package com.getjavajob.training.algo.init.cherkashinA;

public class TaskCh11N158 {

    public static int[] deleteSameElements(int[] arr) {
        for (int i = arr.length - 1; i >= 0; i--) {
            for (int j = i - 1; j >= 0; j--) {
                if (arr[i] == arr[j]) {
                    for (int k = i; k < arr.length - 1; k++) {
                        arr[k] = arr[k + 1];
                        arr[k + 1] = 0;
                    }

                }
            }
        }
        return arr;
    }

}
