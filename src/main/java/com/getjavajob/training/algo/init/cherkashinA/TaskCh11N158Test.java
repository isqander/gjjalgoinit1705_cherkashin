package com.getjavajob.training.algo.init.cherkashinA;

/**
 * Created by Александр on 12.04.2017.
 */

import static com.getjavajob.training.algo.init.cherkashinA.TaskCh11N158.deleteSameElements;
import static com.getjavajob.training.algo.util.Assert.assertEquals;

public class TaskCh11N158Test {

    public static void main(String[] args) {
        testDeleteSameElements();
    }

    public static void testDeleteSameElements() {
        int[] arr = {12, 65, 57, 0, 57, 12, 34, 12, 54, 0};
        int[] newArr = {12, 65, 57, 0, 34, 54, 0, 0, 0, 0};
        assertEquals("TaskCh11N158Test.testDeleteSameElements", newArr, deleteSameElements(arr));
    }

}

