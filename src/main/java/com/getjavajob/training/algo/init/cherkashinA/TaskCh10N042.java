/**
 * Created by Александр on 16.03.2017.
 */

package com.getjavajob.training.algo.init.cherkashinA;

public class TaskCh10N042 {

    public static double pow(double x, int n) {
        if (n == 1) {
            return x;
        }
        return pow(x, n - 1) * x;
    }

}
