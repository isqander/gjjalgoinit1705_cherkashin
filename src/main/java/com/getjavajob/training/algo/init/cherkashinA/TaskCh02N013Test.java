package com.getjavajob.training.algo.init.cherkashinA;

/**
 * Created by Александр on 05.04.2017.
 */

import static com.getjavajob.training.algo.init.cherkashinA.TaskCh02N013.reverse;
import static com.getjavajob.training.algo.util.Assert.assertEquals;

public class TaskCh02N013Test {
    public static void main(String[] args) {
        testReverse();
    }

    public static void testReverse() {
        assertEquals("TaskCh02N013Test.testReverse", 321, reverse(123));
    }

}
