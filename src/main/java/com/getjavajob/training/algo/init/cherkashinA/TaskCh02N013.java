/**
 * Created by Александр on 12.03.2017.
 */

package com.getjavajob.training.algo.init.cherkashinA;

public class TaskCh02N013 {

    public static int reverse(int input) {
        int firstFig = input / 100;
        int twoFig = input % 100;
        int secontFig = twoFig / 10;
        int thirdFig = twoFig % 10;
        return firstFig + secontFig * 10 + thirdFig * 100;
    }

}
