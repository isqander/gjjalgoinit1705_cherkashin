/**
 * Created by Александр on 20.03.2017.
 */

package com.getjavajob.training.algo.init.cherkashinA;

import java.text.SimpleDateFormat;
import java.util.*;

import static java.util.Calendar.YEAR;

public class TaskCh13N012 {

    public static void main(String[] args) {
        Database employees = new Database();
        employees.addEmployee(new Employee("Ivanov", "Ivan", "Ivanovich", "Moscow", new GregorianCalendar(2014, 2, 21)));
        employees.addEmployee(new Employee("Petrov", "Ivan", "Ivanovich", "Moscow", new GregorianCalendar(2014, 2, 21)));
        employees.addEmployee(new Employee("Sidorov", "Ivan", "Moscow", new GregorianCalendar(2015, 11, 21)));
        employees.addEmployee(new Employee("Ivanov1", "Ivan", "Moscow", new GregorianCalendar(2003, 2, 21)));
        employees.addEmployee(new Employee("Ivanov2", "Ivan", "Moscow", new GregorianCalendar(2016, 2, 21)));
        employees.addEmployee(new Employee("Ivanov3", "Ivan", "Ivanovich", "Moscow", new GregorianCalendar(2016, 4, 2)));
        employees.addEmployee(new Employee("Ivanov4", "Ivan", "Ivanovich", "Moscow", new GregorianCalendar(2017, 4, 1)));
        employees.addEmployee(new Employee("Ivanov5", "Ivan", "Ivanovich", "Moscow", new GregorianCalendar(2013, 0, 21)));
        employees.addEmployee(new Employee("Ivanov6", "Ivan", "Ivanovich", "Moscow", new GregorianCalendar(2012, 4, 21)));
        employees.addEmployee(new Employee("Ivanov7", "Ivan", "Ivanovich", "Moscow", new GregorianCalendar(2014, 5, 21)));
        employees.addEmployee(new Employee("Ivanov8", "Ivan", "Ivanovich", "Moscow", new GregorianCalendar(2015, 5, 21)));
        employees.addEmployee(new Employee("Ivanov9", "Ivan", "Ivanovich", "Moscow", new GregorianCalendar(2014, 4, 21)));
        employees.addEmployee(new Employee("Ivanov10", "Ivan", "Ivanovich", "Moscow", new GregorianCalendar(2010, 3, 21)));
        employees.addEmployee(new Employee("Ivanov11", "Ivan", "Ivanovich", "Moscow", new GregorianCalendar(2014, 1, 21)));
        employees.addEmployee(new Employee("Ivanov12", "Ivan", "Ivanovich", "Moscow", new GregorianCalendar(2010, 2, 21)));
        employees.addEmployee(new Employee("Ivanov13", "Ivan", "Ivanovich", "Moscow", new GregorianCalendar(2004, 2, 21)));
        employees.addEmployee(new Employee("Ivanov14", "Ivan", "Ivanovich", "Moscow", new GregorianCalendar(2004, 1, 21)));
        employees.addEmployee(new Employee("Ivanov15", "Ivan", "Ivanovich", "Moscow", new GregorianCalendar(2017, 1, 21)));
        employees.addEmployee(new Employee("Ivanov16", "Ivan", "Ivanovich", "Moscow", new GregorianCalendar(2000, 9, 21)));
        employees.addEmployee(new Employee("Ivanov17", "Ivan", "Ivanovich", "Moscow", new GregorianCalendar(2010, 2, 21)));

        Scanner scan = new Scanner(System.in);
        System.out.println("Input the number of years to find employees working for:");
        System.out.println(employees.findByWorkingYears(scan.nextInt()));
        System.out.println();
        System.out.println("Input the substring to find employees:");
        System.out.println(employees.findBySubstring(scan.next()));
    }

}

class Employee extends java.lang.Object {
    private String surname;
    private String name;
    private String fathersname;
    private String adress;
    private Calendar admission;

    public Employee(String surname, String name, String fathersname, String adress, Calendar admission) {
        this.surname = surname;
        this.name = name;
        this.fathersname = fathersname;
        this.adress = adress;
        this.admission = admission;
    }

    public Employee(String surname, String name, String adress, Calendar admission) {
        this(surname, name, "", adress, admission);
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getFathersname() {
        return fathersname;
    }

    public void setFathersname(String fathersname) {
        this.fathersname = fathersname;
    }

    public String getAdress() {
        return adress;
    }

    public void setAdress(String adress) {
        this.adress = adress;
    }

    public Calendar getAdmission() {
        return admission;
    }

    public void setAdmission(Calendar admission) {
        this.admission = admission;
    }

    @Override
    public String toString() {
        SimpleDateFormat format1 = new SimpleDateFormat("dd.MM.yyyy");
        return surname + " " + name + " " + fathersname + ", " + adress + " hired " + format1.format(admission.getTime());
    }

    public int workingYearsNumber() {
        Calendar today = new GregorianCalendar();
        return today.get(YEAR) - admission.get(YEAR);
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (this.getClass() != obj.getClass()) {
            return false;
        } else {
            Employee comparedEmp = (Employee) obj;
            return this.surname.equals(comparedEmp.surname) && this.name.equals(comparedEmp.name) && this.fathersname.equals(comparedEmp.fathersname) && this.adress.equals(comparedEmp.adress) && this.admission.equals(comparedEmp.admission);
        }
    }
}

class Database {
    private List<Employee> employees;

    Database() {
        employees = new ArrayList<>();
    }

    public void addEmployee(Employee newEmployee) {
        employees.add(newEmployee);
    }

    public List<Object> findBySubstring(String substring) {
        List<Object> foundEmoloyees = new ArrayList<>();
        for (Employee emp : employees) {
            String lowerCaseEmp = (emp.getSurname() + " " + emp.getName() + " " + emp.getFathersname()).toLowerCase();
            if (lowerCaseEmp.contains(substring)) {
                foundEmoloyees.add(emp);
            }
        }
        return foundEmoloyees;
    }

    public List<Object> findByWorkingYears(int workingYears) {
        List<Object> foundEmoloyees = new ArrayList<>();
        for (Employee emp : employees) {
            if (emp.workingYearsNumber() >= workingYears) {
                foundEmoloyees.add(emp);
            }
        }
        return foundEmoloyees;
    }

}