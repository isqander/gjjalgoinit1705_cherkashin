/**
 * Created by Александр on 14.03.2017.
 */

package com.getjavajob.training.algo.init.cherkashinA;

import java.util.Scanner;

public class TaskCh05N010 {

    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        double course = scan.nextDouble();
        for (int i = 0; i < 20; i++) {
            System.out.printf("%d\t%.2f%n", i + 1, makeCourseTable(course)[i]);
        }
    }

    public static double[] makeCourseTable(double course) {
        double[] table = new double[20];
        for (int i = 0; i < table.length; i++) {
            table[i] = (i + 1) * course;
        }
        return table;
    }

}
