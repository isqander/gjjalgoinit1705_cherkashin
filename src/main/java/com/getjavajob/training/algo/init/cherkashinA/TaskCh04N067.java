/**
 * Created by Александр on 14.03.2017.
 */

package com.getjavajob.training.algo.init.cherkashinA;

public class TaskCh04N067 {

    public static String countDay(int k) {
        int weekDay = k % 7;
        switch (weekDay) {
            case 1:
            case 2:
            case 3:
            case 4:
            case 5:
                return "Workday";
            case 6:
            case 0:
                return "Weekend";
            default:
                return "Miracle";
        }
    }

}
