package com.getjavajob.training.algo.init.cherkashinA;

/**
 * Created by Александр on 09.04.2017.
 */

import static com.getjavajob.training.algo.init.cherkashinA.TaskCh09N015.returnKsimbol;
import static com.getjavajob.training.algo.util.Assert.assertEquals;

public class TaskCh09N015Test {

    public static void main(String[] args) {
        testReturnKsimbol();
    }

    public static void testReturnKsimbol() {
        assertEquals("TaskCh09N015Test.testReturnKsimbol", 'e', returnKsimbol("qwerty", 3));
    }

}
