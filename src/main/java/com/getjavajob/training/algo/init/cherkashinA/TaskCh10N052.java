/**
 * Created by Александр on 17.03.2017.
 */

package com.getjavajob.training.algo.init.cherkashinA;

import static java.lang.Math.log10;
import static java.lang.Math.pow;

public class TaskCh10N052 {

    static int output;

    public static void main(String[] args) {
        System.out.println(reverseNumber(12345));
    }

    public static int reverseNumber(int n) {
        output += (n % 10) * pow(10, (int) log10(n));
        if (n < 10) {
            return output;
        }
        reverseNumber(n / 10);
        return output;
    }

}
