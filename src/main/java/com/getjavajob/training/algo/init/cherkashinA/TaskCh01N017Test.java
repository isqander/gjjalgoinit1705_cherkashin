package com.getjavajob.training.algo.init.cherkashinA;

/**
 * Created by Александр on 05.04.2017.
 */

import static com.getjavajob.training.algo.init.cherkashinA.TaskCh01N017.*;
import static com.getjavajob.training.algo.util.Assert.assertEquals;
import static java.lang.Math.PI;
import static java.lang.Math.sqrt;

public class TaskCh01N017Test {
    public static void main(String[] args) {
        testO();
        testP();
        testR();
        testS();
    }

    public static void testO() {
        assertEquals("TaskCh01N017Test.testO", 0, taskO(PI / 2));
    }

    public static void testP() {
        assertEquals("TaskCh01N017Test.testP", 0.5, taskP(1, 1, 1, 2));
    }

    public static void testR() {
        assertEquals("TaskCh01N017Test.testR", sqrt(2) / 2, taskR(1));
    }

    public static void testS() {
        assertEquals("TaskCh01N017Test.testS", 3, taskS(-2));
    }

}
