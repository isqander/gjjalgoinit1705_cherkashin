/**
 * Created by Александр on 15.03.2017.
 */

package com.getjavajob.training.algo.init.cherkashinA;

public class TaskCh09N022 {

    public static String returnFirstHalf(String word) {
        return word.substring(0, word.length() / 2);
    }

}
