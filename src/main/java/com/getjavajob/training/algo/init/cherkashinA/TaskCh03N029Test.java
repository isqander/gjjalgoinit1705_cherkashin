package com.getjavajob.training.algo.init.cherkashinA;

/**
 * Created by Александр on 14.05.2017.
 */

import static com.getjavajob.training.algo.init.cherkashinA.TaskCh03N029.*;
import static com.getjavajob.training.algo.util.Assert.assertEquals;

public class TaskCh03N029Test {
    public static void main(String[] args) {
        testTaskAEvenEven();
        testTaskAEvenOdd();
        testTaskAOddEven();
        testTaskAOddOdd();

        testTaskBLess20Less20();
        testTaskBMore20Less20();
        testTaskBLess20More20();
        testTaskBMore20More20();

        testTaskCEqual0Equal0();
        testTaskCEqual0NotEqual0();
        testTaskCNotEqual0Equal0();
        testTaskCNotEqual0NotEqual0();

        testTaskDPositivePositivePositive();
        testTaskDPositivePositiveNegative();
        testTaskDPositiveNegativePositive();
        testTaskDPositiveNegativeNegative();
        testTaskDNegativePositivePositive();
        testTaskDNegativePositiveNegative();
        testTaskDNegativeNegativePositive();
        testTaskDNegativeNegativeNegative();

        testTaskEDivisible5Divisible5Divisible5();
        testTaskEDivisible5Divisible5Undivisible5();
        testTaskEDivisible5Undivisible5Divisible5();
        testTaskEDivisible5Undivisible5Undivisible5();
        testTaskEUndivisible5Divisible5Divisible5();
        testTaskEUndivisible5Divisible5Undivisible5();
        testTaskEUndivisible5Undivisible5Divisible5();
        testTaskEUndivisible5Undivisible5Undivisible5();

        testTaskFMore100More100More100();
        testTaskFMore100More100Less100();
        testTaskFMore100Less100More100();
        testTaskFMore100Less100Less100();
        testTaskFLess100More100More100();
        testTaskFLess100More100Less100();
        testTaskFLess100Less100More100();
        testTaskFLess100Less100Less100();

    }

    //TaskA
    public static void testTaskAEvenEven() {
        assertEquals("TaskCh03N029Test.testTaskAEvenEven", false, taskA(132, 342));
    }

    public static void testTaskAEvenOdd() {
        assertEquals("TaskCh03N029Test.testTaskAEvenEven", false, taskA(132, 341));
    }

    public static void testTaskAOddEven() {
        assertEquals("TaskCh03N029Test.testTaskAEvenEven", false, taskA(131, 342));
    }

    public static void testTaskAOddOdd() {
        assertEquals("TaskCh03N029Test.testTaskAEvenEven", true, taskA(131, 341));
    }

    //TaskB
    public static void testTaskBLess20Less20() {
        assertEquals("TaskCh03N029Test.testTaskBLess20Less20", false, taskB(1, 1));
    }

    public static void testTaskBMore20Less20() {
        assertEquals("TaskCh03N029Test.testTaskBMore20Less20", true, taskB(22, 1));
    }

    public static void testTaskBLess20More20() {
        assertEquals("TaskCh03N029Test.testTaskBLess20More20", true, taskB(1, 22));
    }

    public static void testTaskBMore20More20() {
        assertEquals("TaskCh03N029Test.testTaskBMore20More20", false, taskB(22, 22));
    }

    //TaskC
    public static void testTaskCEqual0Equal0() {
        assertEquals("TaskCh03N029Test.testTaskCEqual0Equal0", true, taskC(0, 0));
    }

    public static void testTaskCEqual0NotEqual0() {
        assertEquals("TaskCh03N029Test.testTaskCEqual0NotEqual0", true, taskC(1, 0));
    }

    public static void testTaskCNotEqual0Equal0() {
        assertEquals("TaskCh03N029Test.testTaskCNotEqual0Equal0", true, taskC(0, 1));
    }

    public static void testTaskCNotEqual0NotEqual0() {
        assertEquals("TaskCh03N029Test.testTaskCNotEqual0NotEqual0", false, taskC(1, 1));
    }

    //TaskD
    public static void testTaskDPositivePositivePositive() {
        assertEquals("TaskCh03N029Test.testTaskDPositivePositivePositive", false, taskD(1, 1, 1));
    }

    public static void testTaskDPositivePositiveNegative() {
        assertEquals("TaskCh03N029Test.testTaskDPositivePositiveNegative", false, taskD(1, 1, -1));
    }

    public static void testTaskDPositiveNegativePositive() {
        assertEquals("TaskCh03N029Test.testTaskDPositiveNegativePositive", false, taskD(1, -1, 1));
    }

    public static void testTaskDPositiveNegativeNegative() {
        assertEquals("TaskCh03N029Test.testTaskDPositiveNegativeNegative", false, taskD(1, -1, -1));
    }

    public static void testTaskDNegativePositivePositive() {
        assertEquals("TaskCh03N029Test.testTaskDNegativePositivePositive", false, taskD(-1, 1, 1));
    }

    public static void testTaskDNegativePositiveNegative() {
        assertEquals("TaskCh03N029Test.testTaskDNegativePositiveNegative", false, taskD(-1, 1, -1));
    }

    public static void testTaskDNegativeNegativePositive() {
        assertEquals("TaskCh03N029Test.testTaskDNegativeNegativePositive", false, taskD(-1, -1, 1));
    }

    public static void testTaskDNegativeNegativeNegative() {
        assertEquals("TaskCh03N029Test.testTaskDNegativeNegativeNegative", true, taskD(-1, -1, -1));
    }

    //TaskE
    public static void testTaskEDivisible5Divisible5Divisible5() {
        assertEquals("TaskCh03N029Test.testTaskEDivisible5Divisible5Divisible5", false, taskE(5, 5, 5));
    }

    public static void testTaskEDivisible5Divisible5Undivisible5() {
        assertEquals("TaskCh03N029Test.testTaskEDivisible5Divisible5Undivisible5", false, taskE(5, 5, 1));
    }

    public static void testTaskEDivisible5Undivisible5Divisible5() {
        assertEquals("TaskCh03N029Test.testTaskEDivisible5Undivisible5Divisible5", false, taskE(5, 1, 5));
    }

    public static void testTaskEDivisible5Undivisible5Undivisible5() {
        assertEquals("TaskCh03N029Test.testTaskEDivisible5Undivisible5Undivisible5", true, taskE(5, 1, 1));
    }

    public static void testTaskEUndivisible5Divisible5Divisible5() {
        assertEquals("TaskCh03N029Test.testTaskEUndivisible5Divisible5Divisible5", false, taskE(1, 5, 5));
    }

    public static void testTaskEUndivisible5Divisible5Undivisible5() {
        assertEquals("TaskCh03N029Test.testTaskEUndivisible5Divisible5Undivisible5", true, taskE(1, 5, 1));
    }

    public static void testTaskEUndivisible5Undivisible5Divisible5() {
        assertEquals("TaskCh03N029Test.testTaskEUndivisible5Undivisible5Divisible5", true, taskE(1, 1, 5));
    }

    public static void testTaskEUndivisible5Undivisible5Undivisible5() {
        assertEquals("TaskCh03N029Test.testTaskEUndivisible5Undivisible5Undivisible5", false, taskE(1, 1, 1));
    }

    //TaskF
    public static void testTaskFMore100More100More100() {
        assertEquals("TaskCh03N029Test.testTaskFMore100More100More100", true, taskF(101, 101, 101));
    }

    public static void testTaskFMore100More100Less100() {
        assertEquals("TaskCh03N029Test.testTaskFMore100More100Less100", true, taskF(101, 101, 1));
    }

    public static void testTaskFMore100Less100More100() {
        assertEquals("TaskCh03N029Test.testTaskFMore100Less100More100", true, taskF(101, 1, 101));
    }

    public static void testTaskFMore100Less100Less100() {
        assertEquals("TaskCh03N029Test.testTaskFMore100Less100Less100", true, taskF(101, 1, 1));
    }

    public static void testTaskFLess100More100More100() {
        assertEquals("TaskCh03N029Test.testTaskFLess100More100More100", true, taskF(1, 101, 101));
    }

    public static void testTaskFLess100More100Less100() {
        assertEquals("TaskCh03N029Test.testTaskFLess100More100Less100", true, taskF(1, 101, 1));
    }

    public static void testTaskFLess100Less100More100() {
        assertEquals("TaskCh03N029Test.testTaskFLess100Less100More100", true, taskF(1, 1, 101));
    }

    public static void testTaskFLess100Less100Less100() {
        assertEquals("TaskCh03N029Test.testTaskFLess100Less100Less100", false, taskF(1, 1, 1));
    }

}
