package com.getjavajob.training.algo.init.cherkashinA;

/**
 * Created by Александр on 13.04.2017.
 */

import static com.getjavajob.training.algo.init.cherkashinA.TaskCh12N024.makeArrayA;
import static com.getjavajob.training.algo.init.cherkashinA.TaskCh12N024.makeArrayB;
import static com.getjavajob.training.algo.util.Assert.assertEquals;

public class TaskCh12N024Test {

    public static void main(String[] args) {
        testMakeArrayA();
        testMakeArrayB();
    }

    public static void testMakeArrayA() {
        int[][] outputArray = new int[][]{
                {1, 1, 1, 1, 1, 1},
                {1, 2, 3, 4, 5, 6},
                {1, 3, 6, 10, 15, 21},
                {1, 4, 10, 20, 35, 56},
                {1, 5, 15, 35, 70, 126},
                {1, 6, 21, 56, 126, 252}
        };
        assertEquals("TaskCh12N024Test.testMakeArrayA", outputArray, makeArrayA(6));
    }

    public static void testMakeArrayB() {
        int[][] outputArray = new int[][]{
                {1, 2, 3, 4, 5, 6},
                {2, 3, 4, 5, 6, 1},
                {3, 4, 5, 6, 1, 2},
                {4, 5, 6, 1, 2, 3},
                {5, 6, 1, 2, 3, 4},
                {6, 1, 2, 3, 4, 5},
        };
        assertEquals("TaskCh12N024Test.testMakeArrayB", outputArray, makeArrayB(6, 6));
    }

}
