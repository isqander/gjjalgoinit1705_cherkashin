/**
 * Created by Александр on 20.03.2017.
 */

package com.getjavajob.training.algo.init.cherkashinA;

import java.util.Arrays;

public class TaskCh12N063 {

    public static final int[][] ARRAY_OF_STUDENTS = fillArray(new int[4][11]);

    public static void main(String[] args) {
        System.out.println(Arrays.toString(averageStudents(ARRAY_OF_STUDENTS)));
    }

    private static int[][] fillArray(int[][] ar) {
        for (int i = 0; i < ar[0].length; i++) {
            ar[0][i] = 25;
            ar[1][i] = 26;
            ar[2][i] = 27;
            ar[3][i] = 28;
        }
        ar[0][5] = 30;
        ar[1][5] = 30;
        ar[2][5] = 31;
        ar[3][5] = 31;
        return ar;
    }

    public static double[] averageStudents(int[][] inputArr) {
        double[] average = new double[11];
        for (int i = 0; i < inputArr[0].length; i++) {
            for (int[] anInputArr : inputArr) {
                average[i] += (double) anInputArr[i] / inputArr.length;
            }
        }
        return average;
    }

}
