/**
 * Created by Александр on 12.03.2017.
 */

package com.getjavajob.training.algo.init.cherkashinA;

public class TaskCh03N029 {

    public static boolean taskA(int x, int y) {
        return x % 2 != 0 && y % 2 != 0;
    }

    public static boolean taskB(int x, int y) {
        return x < 20 ^ y < 20;
    }

    public static boolean taskC(int x, int y) {
        return x == 0 || y == 0;
    }

    public static boolean taskD(int x, int y, int z) {
        return x < 0 && y < 0 && z < 0;
    }

    public static boolean taskE(int x, int y, int z) {
        boolean xDiv = x % 5 == 0;
        boolean yDiv = y % 5 == 0;
        boolean zDiv = z % 5 == 0;
        return xDiv ^ yDiv ^ zDiv && !(xDiv && yDiv && zDiv);
    }

    public static boolean taskF(int x, int y, int z) {
        return x > 100 || y > 100 || z > 100;
    }

}