/**
 * Created by Александр on 14.03.2017.
 */

package com.getjavajob.training.algo.init.cherkashinA;

import java.util.Scanner;

public class TaskCh04N115 {

    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        System.out.println("Input the year:");
        System.out.println("This is a year of " + countYear(Integer.parseInt(scan.nextLine())));
    }

    public static String countYear(int year) {
        final int fullCycle = 60;
        final int numberOfCycles = 60;
        final int datumYear = 1983;
        int countYear = year - datumYear + fullCycle * numberOfCycles;
        int animalNumber = countYear % 12;
        String animal;
        switch (animalNumber) {
            case 1:
                animal = "Rat";
                break;
            case 2:
                animal = "Cow";
                break;
            case 3:
                animal = "Tiger";
                break;
            case 4:
                animal = "Rabbit";
                break;
            case 5:
                animal = "Dragon";
                break;
            case 6:
                animal = "Snake";
                break;
            case 7:
                animal = "Horse";
                break;
            case 8:
                animal = "Sheep";
                break;
            case 9:
                animal = "Monkey";
                break;
            case 10:
                animal = "Cock";
                break;
            case 11:
                animal = "Dog";
                break;
            case 0:
                animal = "Pig";
                break;
            default:
                animal = "";
                break;
        }
        int colorNumber = countYear % 10;
        String color;
        switch (colorNumber) {
            case 1:
            case 2:
                color = "Green";
                break;
            case 3:
            case 4:
                color = "Red";
                break;
            case 5:
            case 6:
                color = "Yellow";
                break;
            case 7:
            case 8:
                color = "White";
                break;
            case 9:
            case 0:
                color = "Black";
                break;
            default:
                color = "";
                break;
        }
        return color + " " + animal;
    }

}
