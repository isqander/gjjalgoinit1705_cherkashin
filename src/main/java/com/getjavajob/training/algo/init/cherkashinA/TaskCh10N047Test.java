package com.getjavajob.training.algo.init.cherkashinA;

/**
 * Created by Александр on 11.04.2017.
 */

import static com.getjavajob.training.algo.init.cherkashinA.TaskCh10N047.fibonacci;
import static com.getjavajob.training.algo.util.Assert.assertEquals;

public class TaskCh10N047Test {

    public static void main(String[] args) {
        testFibonacci();
    }

    public static void testFibonacci() {
        assertEquals("TaskCh10N047Test.testFibonacci", 8, fibonacci(6));
    }

}
