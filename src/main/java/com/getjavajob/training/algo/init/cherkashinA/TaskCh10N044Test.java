package com.getjavajob.training.algo.init.cherkashinA;

/**
 * Created by Александр on 11.04.2017.
 */

import static com.getjavajob.training.algo.init.cherkashinA.TaskCh10N044.countRoot;
import static com.getjavajob.training.algo.util.Assert.assertEquals;

public class TaskCh10N044Test {

    public static void main(String[] args) {
        testCountRoot();
    }

    public static void testCountRoot() {
        assertEquals("TaskCh10N044Test.testCountRoot", 5, countRoot(158));
    }

}