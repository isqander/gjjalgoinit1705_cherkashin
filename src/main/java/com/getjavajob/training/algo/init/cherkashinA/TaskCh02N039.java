/**
 * Created by Александр on 12.03.2017.
 */

package com.getjavajob.training.algo.init.cherkashinA;

public class TaskCh02N039 {
    final static int SECONDS_IN_DAY = 12 * 60 * 60;
    final static int ALLDEGREE = 360;
    final static int SECONDS_IN_MINUTE = 60;
    final static int MINUTES_IN_HOUR = 60;
    final static int HOURS_IN_DAY = 60;

    public static int findDegree(int h, int m, int s) {
        int secondsNow = s + m * SECONDS_IN_MINUTE + h % HOURS_IN_DAY * MINUTES_IN_HOUR * SECONDS_IN_MINUTE;
        return ALLDEGREE * secondsNow / SECONDS_IN_DAY;
    }

}
