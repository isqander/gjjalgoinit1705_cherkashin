/**
 * Created by Александр on 14.03.2017.
 */

package com.getjavajob.training.algo.init.cherkashinA;

public class TaskCh05N038 {

    public static void main(String[] args) {
        System.out.println(findTrack(100) + " " + findPosition(100));
    }

    public static double findPosition(int n) {
        double position = 0;
        for (double i = 1; i < (n + 1) / 2 + 1; i++) {
            position += 1 / (2 * i - 1);
        }
        for (double i = 1; i < (n + 1) / 2 + 1; i++) {
            position -= 1 / (2 * i);
        }
        return position;
    }

    public static double findTrack(int n) {
        double track = 0;
        for (double i = 1; i < (n + 1) / 2 + 1; i++) {
            track += 1 / (2 * i - 1);
        }
        for (double i = 1; i < (n + 1) / 2 + 1; i++) {
            track += 1 / (2 * i);
        }
        return track;
    }

}
