/**
 * Created by Александр on 20.03.2017.
 */

package com.getjavajob.training.algo.init.cherkashinA;

public class TaskCh12N023 {

    public static void main(String[] args) {
        printArray(makeArrayA(9));
        printArray(makeArrayB(9));
        printArray(makeArrayC(9));
    }

    public static void printArray(int[][] ar) {
        for (int[] anAr : ar) {
            System.out.println();
            for (int j = 0; j < ar[0].length; j++) {
                System.out.print(anAr[j] + "\t");
            }
        }
        System.out.println();
    }

    public static int[][] makeArrayA(int n) {
        int[][] arrayA = new int[n][n];
        for (int i = 0; i < arrayA.length; i++) {
            for (int j = 0; j < arrayA.length; j++) {
                if (i == j || i == n - 1 - j) {
                    arrayA[i][j] = 1;
                }
            }
        }
        return arrayA;
    }

    public static int[][] makeArrayB(int n) {
        int[][] arrayB = new int[n][n];
        for (int i = 0; i < arrayB.length; i++) {
            for (int j = 0; j < arrayB.length; j++) {
                if (i == j || i == n - 1 - j || i == n / 2 || j == n / 2) {
                    arrayB[i][j] = 1;
                }
            }
        }
        return arrayB;
    }

    public static int[][] makeArrayC(int n) {
        int[][] arrayC = new int[n][n];
        for (int i = 0; i < arrayC.length; i++) {
            for (int j = 0; j < arrayC.length; j++) {
                if (i <= j && n - i > j || j <= i && j >= n - i - 1) {
                    arrayC[i][j] = 1;
                }
            }
        }
        return arrayC;
    }

}
