/**
 * Created by Александр on 17.03.2017.
 */

package com.getjavajob.training.algo.init.cherkashinA;

public class TaskCh10N045 {

    static int findNmember(int firstMember, int difference, int n) {
        int newN = n;
        newN--;
        if (newN == 0) {
            return firstMember;
        } else {
            return findNmember(firstMember, difference, newN) + difference;
        }
    }

    static int sumNmembers(int firstMember, int difference, int n) {
        int newN = n;
        newN--;
        if (newN == 0) {
            return firstMember;
        } else {
            return sumNmembers(firstMember, difference, newN) + findNmember(firstMember, difference, newN) + difference;
        }
    }

}
