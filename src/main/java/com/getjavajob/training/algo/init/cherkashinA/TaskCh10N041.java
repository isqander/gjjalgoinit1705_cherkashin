/**
 * Created by Александр on 16.03.2017.
 */

package com.getjavajob.training.algo.init.cherkashinA;

public class TaskCh10N041 {

    public static int factorial(int a) {
        if (a == 1) {
            return 1;
        }
        return factorial(a - 1) * a;
    }

}
