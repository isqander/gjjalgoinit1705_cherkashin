/**
 * Created by Александр on 20.03.2017.
 */

package com.getjavajob.training.algo.init.cherkashinA;

import static com.getjavajob.training.algo.init.cherkashinA.TaskCh12N023.printArray;

public class TaskCh12N234 {

    public static final int[][] INPUT_ARRAY = new int[][]{
            {1, 2, 3, 4, 5, 6},
            {2, 3, 4, 5, 6, 1},
            {3, 4, 5, 6, 1, 2},
            {4, 5, 6, 1, 2, 3},
            {5, 6, 1, 2, 3, 4},
            {6, 1, 2, 3, 4, 5},
    };

    public static void main(String[] args) {
        printArray(deleteString(INPUT_ARRAY, 3));
        printArray(deleteColumn(INPUT_ARRAY, 3));
        printArray(INPUT_ARRAY);
    }

    public static int[][] cloneTwoDimArray(int[][] inputArray) {
        int[][] outputArray = new int[inputArray.length][inputArray[0].length];
        for (int i = 0; i < inputArray.length; i++) {
            System.arraycopy(inputArray[i], 0, outputArray[i], 0, inputArray[i].length);
        }
        return outputArray;
    }

    public static int[][] deleteColumn(int[][] inputArray, int stringNumber) {
        int[][] outputArray = cloneTwoDimArray(inputArray);
        for (int i = 0; i < inputArray.length; i++) {
            for (int j = 0; j < inputArray[0].length; j++) {
                if (j == stringNumber) {
                    System.arraycopy(inputArray[i], stringNumber + 1, outputArray[i], stringNumber, inputArray[0].length - 1 - stringNumber);
                    outputArray[i][inputArray[0].length - 1] = 0;
                }
            }
        }
        return outputArray;
    }

    public static int[][] deleteString(int[][] inputArray, int columnNumber) {
        int[][] outputArray = cloneTwoDimArray(inputArray);
        for (int i = 0; i < inputArray[0].length; i++) {
            for (int j = 0; j < inputArray.length; j++) {
                if (j == columnNumber) {
                    for (int n = columnNumber; n < inputArray.length - 1; n++) {
                        outputArray[n][i] = inputArray[n + 1][i];
                    }
                    outputArray[inputArray.length - 1][i] = 0;
                }
            }
        }
        return outputArray;
    }

}
