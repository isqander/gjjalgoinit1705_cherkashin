package com.getjavajob.training.algo.init.cherkashinA;

/**
 * Created by Александр on 13.04.2017.
 */

import static com.getjavajob.training.algo.init.cherkashinA.TaskCh12N023.*;
import static com.getjavajob.training.algo.util.Assert.assertEquals;

public class TaskCh12N023Test {

    public static void main(String[] args) {
        testMakeArrayA();
        testMakeArrayB();
        testMakeArrayC();
    }

    public static void testMakeArrayA() {
        int[][] arrA = new int[][]{
                {1, 0, 0, 0, 0, 0, 1},
                {0, 1, 0, 0, 0, 1, 0},
                {0, 0, 1, 0, 1, 0, 0},
                {0, 0, 0, 1, 0, 0, 0},
                {0, 0, 1, 0, 1, 0, 0},
                {0, 1, 0, 0, 0, 1, 0},
                {1, 0, 0, 0, 0, 0, 1}
        };
        assertEquals("TaskCh12N023Test.testMakeArrayA", arrA, makeArrayA(7));
    }

    public static void testMakeArrayB() {
        int[][] arrB = new int[][]{
                {1, 0, 0, 1, 0, 0, 1},
                {0, 1, 0, 1, 0, 1, 0},
                {0, 0, 1, 1, 1, 0, 0},
                {1, 1, 1, 1, 1, 1, 1},
                {0, 0, 1, 1, 1, 0, 0},
                {0, 1, 0, 1, 0, 1, 0},
                {1, 0, 0, 1, 0, 0, 1}
        };
        assertEquals("TaskCh12N023Test.testMakeArrayB", arrB, makeArrayB(7));
    }

    public static void testMakeArrayC() {
        int[][] arrC = new int[][]{
                {1, 1, 1, 1, 1, 1, 1},
                {0, 1, 1, 1, 1, 1, 0},
                {0, 0, 1, 1, 1, 0, 0},
                {0, 0, 0, 1, 0, 0, 0},
                {0, 0, 1, 1, 1, 0, 0},
                {0, 1, 1, 1, 1, 1, 0},
                {1, 1, 1, 1, 1, 1, 1}
        };
        assertEquals("TaskCh12N023Test.testMakeArrayC", arrC, makeArrayC(7));
    }

}

