package com.getjavajob.training.algo.init.cherkashinA;

/**
 * Created by Александр on 01.05.2017.
 */

import java.util.ArrayList;
import java.util.GregorianCalendar;
import java.util.List;

import static com.getjavajob.training.algo.util.Assert.assertEquals;

public class TaskCh13N012Test {

    public static void main(String[] args) {
        testSubstringSearsh();
        testFindByWorkingYears();
    }

    public static void testSubstringSearsh() {
        Database empList = new Database();
        empList.addEmployee(new Employee("Ivanov", "Ivan", "Ivanovich", "Moscow", new GregorianCalendar(2014, 2, 21)));
        empList.addEmployee(new Employee("Petrov", "Ivan", "Ivanovich", "Moscow", new GregorianCalendar(2014, 2, 21)));
        empList.addEmployee(new Employee("Sidorov", "Ivan", "Moscow", new GregorianCalendar(2015, 11, 21)));
        empList.addEmployee(new Employee("Ivanov1", "Ivan", "Moscow", new GregorianCalendar(2003, 2, 21)));
        empList.addEmployee(new Employee("Ivanov2", "Ivan", "Moscow", new GregorianCalendar(2016, 2, 21)));
        empList.addEmployee(new Employee("Ivanov3", "Ivan", "Ivanovich", "Moscow", new GregorianCalendar(2016, 4, 2)));
        empList.addEmployee(new Employee("Ivanov4", "Ivan", "Ivanovich", "Moscow", new GregorianCalendar(2017, 4, 1)));
        empList.addEmployee(new Employee("Ivanov5", "Ivan", "Ivanovich", "Moscow", new GregorianCalendar(2013, 0, 21)));
        empList.addEmployee(new Employee("Ivanov6", "Ivan", "Ivanovich", "Moscow", new GregorianCalendar(2012, 4, 21)));
        empList.addEmployee(new Employee("Ivanov7", "Ivan", "Ivanovich", "Moscow", new GregorianCalendar(2014, 5, 21)));
        empList.addEmployee(new Employee("Ivanov8", "Ivan", "Ivanovich", "Moscow", new GregorianCalendar(2015, 5, 21)));
        empList.addEmployee(new Employee("Ivanov9", "Ivan", "Ivanovich", "Moscow", new GregorianCalendar(2014, 4, 21)));
        empList.addEmployee(new Employee("Ivanov10", "Ivan", "Ivanovich", "Moscow", new GregorianCalendar(2010, 3, 21)));
        empList.addEmployee(new Employee("Ivanov11", "Ivan", "Ivanovich", "Moscow", new GregorianCalendar(2014, 1, 21)));
        empList.addEmployee(new Employee("Ivanov12", "Ivan", "Ivanovich", "Moscow", new GregorianCalendar(2010, 2, 21)));
        empList.addEmployee(new Employee("Ivanov13", "Ivan", "Ivanovich", "Moscow", new GregorianCalendar(2004, 2, 21)));
        empList.addEmployee(new Employee("Ivanov14", "Ivan", "Ivanovich", "Moscow", new GregorianCalendar(2004, 1, 21)));
        empList.addEmployee(new Employee("Ivanov15", "Ivan", "Ivanovich", "Moscow", new GregorianCalendar(2017, 1, 21)));
        empList.addEmployee(new Employee("Ivanov16", "Ivan", "Ivanovich", "Moscow", new GregorianCalendar(2000, 9, 21)));
        empList.addEmployee(new Employee("Ivanov17", "Ivan", "Ivanovich", "Moscow", new GregorianCalendar(2010, 2, 21)));

        List<Object> empListExpected = new ArrayList<>();
        empListExpected.add(new Employee("Petrov", "Ivan", "Ivanovich", "Moscow", new GregorianCalendar(2014, 2, 21)));

        assertEquals("TaskCh13N012Test.testSubstringSearsh", empListExpected, empList.findBySubstring("petr"));
    }

    public static void testFindByWorkingYears() {
        Database empList = new Database();
        empList.addEmployee(new Employee("Ivanov", "Ivan", "Ivanovich", "Moscow", new GregorianCalendar(2014, 2, 21)));
        empList.addEmployee(new Employee("Petrov", "Ivan", "Ivanovich", "Moscow", new GregorianCalendar(2014, 2, 21)));
        empList.addEmployee(new Employee("Sidorov", "Ivan", "Moscow", new GregorianCalendar(2015, 11, 21)));
        empList.addEmployee(new Employee("Ivanov1", "Ivan", "Moscow", new GregorianCalendar(2003, 2, 21)));
        empList.addEmployee(new Employee("Ivanov2", "Ivan", "Moscow", new GregorianCalendar(2016, 2, 21)));
        empList.addEmployee(new Employee("Ivanov3", "Ivan", "Ivanovich", "Moscow", new GregorianCalendar(2016, 4, 2)));
        empList.addEmployee(new Employee("Ivanov4", "Ivan", "Ivanovich", "Moscow", new GregorianCalendar(2017, 4, 1)));
        empList.addEmployee(new Employee("Ivanov5", "Ivan", "Ivanovich", "Moscow", new GregorianCalendar(2013, 0, 21)));
        empList.addEmployee(new Employee("Ivanov6", "Ivan", "Ivanovich", "Moscow", new GregorianCalendar(2012, 4, 21)));
        empList.addEmployee(new Employee("Ivanov7", "Ivan", "Ivanovich", "Moscow", new GregorianCalendar(2014, 5, 21)));
        empList.addEmployee(new Employee("Ivanov8", "Ivan", "Ivanovich", "Moscow", new GregorianCalendar(2015, 5, 21)));
        empList.addEmployee(new Employee("Ivanov9", "Ivan", "Ivanovich", "Moscow", new GregorianCalendar(2014, 4, 21)));
        empList.addEmployee(new Employee("Ivanov10", "Ivan", "Ivanovich", "Moscow", new GregorianCalendar(2010, 3, 21)));
        empList.addEmployee(new Employee("Ivanov11", "Ivan", "Ivanovich", "Moscow", new GregorianCalendar(2014, 1, 21)));
        empList.addEmployee(new Employee("Ivanov12", "Ivan", "Ivanovich", "Moscow", new GregorianCalendar(2010, 2, 21)));
        empList.addEmployee(new Employee("Ivanov13", "Ivan", "Ivanovich", "Moscow", new GregorianCalendar(2004, 2, 21)));
        empList.addEmployee(new Employee("Ivanov14", "Ivan", "Ivanovich", "Moscow", new GregorianCalendar(2004, 1, 21)));
        empList.addEmployee(new Employee("Ivanov15", "Ivan", "Ivanovich", "Moscow", new GregorianCalendar(2017, 1, 21)));
        empList.addEmployee(new Employee("Ivanov16", "Ivan", "Ivanovich", "Moscow", new GregorianCalendar(2000, 9, 21)));
        empList.addEmployee(new Employee("Ivanov17", "Ivan", "Ivanovich", "Moscow", new GregorianCalendar(2010, 2, 21)));

        ArrayList<Object> empListExpected = new ArrayList<>();
        empListExpected.add(new Employee("Ivanov1", "Ivan", "Moscow", new GregorianCalendar(2003, 2, 21)));
        empListExpected.add(new Employee("Ivanov13", "Ivan", "Ivanovich", "Moscow", new GregorianCalendar(2004, 2, 21)));
        empListExpected.add(new Employee("Ivanov14", "Ivan", "Ivanovich", "Moscow", new GregorianCalendar(2004, 1, 21)));
        empListExpected.add(new Employee("Ivanov16", "Ivan", "Ivanovich", "Moscow", new GregorianCalendar(2000, 9, 21)));

        assertEquals("TaskCh13N012Test.testFindByWorkingYears", empListExpected, empList.findByWorkingYears(10));
    }

}
