package com.getjavajob.training.algo.init.cherkashinA;

/**
 * Created by Александр on 07.04.2017.
 */

import static com.getjavajob.training.algo.init.cherkashinA.TaskCh04N115.countYear;
import static com.getjavajob.training.algo.util.Assert.assertEquals;

public class TaskCh04N115Test {

    public static void main(String[] args) {
        testCountYearA();
        testCountYearB();
    }

    public static void testCountYearA() {
        assertEquals("TaskCh04N115TestCountYearA", "Red Tiger", countYear(1986));
    }

    public static void testCountYearB() {
        assertEquals("TaskCh04N115TestCountYearB", "Black Pig", countYear(1983));
    }

}
