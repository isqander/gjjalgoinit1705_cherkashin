/**
 * Created by Александр on 16.03.2017.
 */

package com.getjavajob.training.algo.init.cherkashinA;

import static com.getjavajob.training.algo.init.cherkashinA.TaskCh10N043.countNumbersA;

public class TaskCh10N044 {

    static int countRoot(int a) {
        if (a < 10) {
            return a;
        } else {
            return countRoot(countNumbersA(a));
        }
    }

}
