package com.getjavajob.training.algo.init.cherkashinA;

/**
 * Created by Александр on 01.05.2017.
 */

import static com.getjavajob.training.algo.init.cherkashinA.TaskCh10N052.reverseNumber;
import static com.getjavajob.training.algo.util.Assert.assertEquals;

public class TaskCh10N052Test {

    public static void main(String[] args) {
        testReverseNumber();
    }

    public static void testReverseNumber() {
        assertEquals("TaskCh10N052Test.testReverseNumber", 54321, reverseNumber(12345));
    }

}
