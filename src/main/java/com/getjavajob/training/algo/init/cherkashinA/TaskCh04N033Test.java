package com.getjavajob.training.algo.init.cherkashinA;

/**
 * Created by Александр on 07.04.2017.
 */

import static com.getjavajob.training.algo.init.cherkashinA.TaskCh04N033.isEven;
import static com.getjavajob.training.algo.init.cherkashinA.TaskCh04N033.isOdd;
import static com.getjavajob.training.algo.util.Assert.assertEquals;

public class TaskCh04N033Test {
    public static void main(String[] args) {
        testIsEven();
        testIsOdd();
    }

    public static void testIsEven() {
        assertEquals("TaskCh04N033Test.testIsEven", false, isEven(135));
    }

    public static void testIsOdd() {
        assertEquals("TaskCh04N033Test.testIsOdd", true, isOdd(135));
    }

}