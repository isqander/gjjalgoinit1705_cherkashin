/**
 * Created by Александр on 15.03.2017.
 */

package com.getjavajob.training.algo.init.cherkashinA;

import java.util.Scanner;

public class TaskCh09N015 {

    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        System.out.println(returnKsimbol(scan.nextLine(), scan.nextInt()));
    }

    public static char returnKsimbol(String word, int k) {
        return word.charAt(k - 1);
    }

}
