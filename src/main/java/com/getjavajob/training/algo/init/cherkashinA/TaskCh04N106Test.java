package com.getjavajob.training.algo.init.cherkashinA;

/**
 * Created by Александр on 07.04.2017.
 */

import static com.getjavajob.training.algo.init.cherkashinA.TaskCh04N106.countSeason;
import static com.getjavajob.training.algo.util.Assert.assertEquals;

public class TaskCh04N106Test {

    public static void main(String[] args) {
        testCountSeason1();
        testCountSeason2();
    }

    public static void testCountSeason1() {
        assertEquals("TaskCh04N106Test.testCountSeason1", "Winter", countSeason(12));
    }

    public static void testCountSeason2() {
        assertEquals("TaskCh04N106Test.testCountSeason2", "Summer", countSeason(7));
    }

}
