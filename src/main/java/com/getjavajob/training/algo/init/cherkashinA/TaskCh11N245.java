/**
 * Created by Александр on 18.03.2017.
 */

package com.getjavajob.training.algo.init.cherkashinA;

import java.util.Arrays;

public class TaskCh11N245 {

    public static void main(String[] args) {
        int[] inArr = {1, -4, 0, 45, 7, -56, -48};
        int[] outputArr = new int[inArr.length];
        int i = 0;
        int j = outputArr.length - 1;
        for (int a : inArr) {
            if (a < 0) {
                outputArr[i] = a;
                i++;
            } else {
                outputArr[j] = a;
                j--;
            }
        }
        System.out.println(Arrays.toString(outputArr));
    }

    public static int[] firstNegative(int[] inputArr) {
        int[] outputArr = new int[inputArr.length];
        int i = 0;
        int j = outputArr.length - 1;
        for (int a : inputArr) {
            if (a < 0) {
                outputArr[i] = a;
                i++;
            } else {
                outputArr[j] = a;
                j--;
            }
        }
        for (int k = i; k > i / 2; k--) {
            int a = outputArr[outputArr.length - k - 1];
            outputArr[outputArr.length - k - 1] = outputArr[outputArr.length - i + k - 1];
            outputArr[outputArr.length - i + k - 1] = a;
        }
        return outputArr;
    }

}
