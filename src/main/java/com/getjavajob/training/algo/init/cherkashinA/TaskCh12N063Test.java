package com.getjavajob.training.algo.init.cherkashinA;

/**
 * Created by Александр on 13.04.2017.
 */

import static com.getjavajob.training.algo.init.cherkashinA.TaskCh12N063.ARRAY_OF_STUDENTS;
import static com.getjavajob.training.algo.init.cherkashinA.TaskCh12N063.averageStudents;
import static com.getjavajob.training.algo.util.Assert.assertEquals;

public class TaskCh12N063Test {

    public static void main(String[] args) {
        testAverageStudents();
    }

    public static void testAverageStudents() {
        assertEquals("TaskCh12N063Test.testAverageStudents", new double[]{26.5, 26.5, 26.5, 26.5, 26.5, 30.5, 26.5, 26.5, 26.5, 26.5, 26.5}, averageStudents(ARRAY_OF_STUDENTS));
    }

}