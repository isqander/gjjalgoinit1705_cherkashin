package com.getjavajob.training.algo.init.cherkashinA;

/**
 * Created by Александр on 09.04.2017.
 */

import static com.getjavajob.training.algo.init.cherkashinA.TaskCh09N107.changeAnO;
import static com.getjavajob.training.algo.util.Assert.assertEquals;

public class TaskCh09N107Test {

    public static void main(String[] args) {
        testChangeAnO1();
        testChangeAnO2();
    }

    public static void testChangeAnO1() {
        assertEquals("TaskCh09N107Test.testChangeAnO1", "Getjovajob makes my cade clean", changeAnO("Getjavajob makes my code clean"));
    }

    public static void testChangeAnO2() {
        assertEquals("TaskCh09N107Test.testChangeAnO2", "There is no letter \"a\"", changeAnO("Hello"));
    }

}

