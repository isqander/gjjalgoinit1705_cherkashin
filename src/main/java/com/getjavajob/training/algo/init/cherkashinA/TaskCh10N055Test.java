package com.getjavajob.training.algo.init.cherkashinA;

/**
 * Created by Александр on 01.05.2017.
 */

import static com.getjavajob.training.algo.init.cherkashinA.TaskCh10N055.cangeNumberSystem;
import static com.getjavajob.training.algo.util.Assert.assertEquals;

public class TaskCh10N055Test {

    public static void main(String[] args) {
        testCangeNumberSystem();
    }

    public static void testCangeNumberSystem() {
        assertEquals("TaskCh10N055Test.testCangeNumberSystem", "4D2", cangeNumberSystem(1234, 16));
    }

}