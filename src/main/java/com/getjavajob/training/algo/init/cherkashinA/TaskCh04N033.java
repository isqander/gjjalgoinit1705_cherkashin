/**
 * Created by Александр on 14.03.2017.
 */

package com.getjavajob.training.algo.init.cherkashinA;

public class TaskCh04N033 {

    public static boolean isEven(int number) {
        return number % 2 == 0;
    }

    public static boolean isOdd(int number) {
        return number % 2 != 0;
    }

}
