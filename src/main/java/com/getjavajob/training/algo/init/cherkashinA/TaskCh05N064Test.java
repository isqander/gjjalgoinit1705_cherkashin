package com.getjavajob.training.algo.init.cherkashinA;

/**
 * Created by Александр on 07.04.2017.
 */

import static com.getjavajob.training.algo.init.cherkashinA.TaskCh05N064.averageDensity;
import static com.getjavajob.training.algo.util.Assert.assertEquals;

public class TaskCh05N064Test {

    public static void main(String[] args) {
        testAverageDensity();
    }

    public static void testAverageDensity() {
        int[] population = {1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12};
        double[] square = {10., 20., 30., 40., 50., 60., 70., 80., 90., 100., 110., 120.};
        assertEquals("TaskCh05N064Test.testAverageDensity", 0.1, averageDensity(population, square));
    }

}
