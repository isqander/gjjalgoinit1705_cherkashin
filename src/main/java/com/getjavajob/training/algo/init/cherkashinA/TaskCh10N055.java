/**
 * Created by Александр on 17.03.2017.
 */

package com.getjavajob.training.algo.init.cherkashinA;

public class TaskCh10N055 {

    private static String output = "";

    static String cangeNumberSystem(int number, int n) {
        char[] numerals = {'0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'A', 'B', 'C', 'D', 'E', 'F'};
        if (number < n) {
            output = numerals[number] + output;
            return output;
        }
        output = numerals[number % n] + output;
        cangeNumberSystem(number / n, n);
        return output;
    }

    public static void main(String[] args) {
        System.out.println(cangeNumberSystem(27, 2));
    }

}
