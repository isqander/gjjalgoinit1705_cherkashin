package com.getjavajob.training.algo.init.cherkashinA;

/**
 * Created by Александр on 09.04.2017.
 */

import static com.getjavajob.training.algo.init.cherkashinA.TaskCh09N022.returnFirstHalf;
import static com.getjavajob.training.algo.util.Assert.assertEquals;

public class TaskCh09N022Test {

    public static void main(String[] args) {
        testReturnFirstHalf();
    }

    public static void testReturnFirstHalf() {
        assertEquals("TaskCh09N022Test.testReturnFirstHalf", "first", returnFirstHalf("firsthalff"));
    }

}
