package com.getjavajob.training.algo.init.cherkashinA;

/**
 * Created by Александр on 11.04.2017.
 */

import static com.getjavajob.training.algo.init.cherkashinA.TaskCh10N043.countNumbersA;
import static com.getjavajob.training.algo.init.cherkashinA.TaskCh10N043.countNumbersB;
import static com.getjavajob.training.algo.util.Assert.assertEquals;

public class TaskCh10N043Test {

    public static void main(String[] args) {
        testCountNumbersA();
        testCountNumbersB();
    }

    public static void testCountNumbersA() {
        assertEquals("TaskCh10N043Test.testCountNumbersA", 9, countNumbersA(135));
    }

    public static void testCountNumbersB() {
        assertEquals("TaskCh10N043Test.testCountNumbersB", 3, countNumbersB(135));
    }

}

