package com.getjavajob.training.algo.init.cherkashinA;

/**
 * Created by Александр on 07.04.2017.
 */

import static com.getjavajob.training.algo.init.cherkashinA.TaskCh05N010.makeCourseTable;
import static com.getjavajob.training.algo.util.Assert.assertEquals;

public class TaskCh05N010Test {

    public static void main(String[] args) {
        testMakeCourseTable();
    }

    public static void testMakeCourseTable() {
        double[] ar = {2, 4, 6, 8, 10, 12, 14, 16, 18, 20, 22, 24, 26, 28, 30, 32, 34, 36, 38, 40};
        assertEquals("TaskCh05N010Test.testMakeCourseTable", ar, makeCourseTable(2));
    }

}