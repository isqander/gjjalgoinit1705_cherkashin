/**
 * Created by Александр on 14.03.2017.
 */

package com.getjavajob.training.algo.init.cherkashinA;

public class TaskCh04N036 {

    public static String showTrafficLight(double time) {
        int interCicleTime = (int) time % 5;
        return interCicleTime < 3 ? "green" : "red";
    }

}
