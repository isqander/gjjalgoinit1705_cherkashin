package com.getjavajob.training.algo.init.cherkashinA;

/**
 * Created by Александр on 11.04.2017.
 */

import static com.getjavajob.training.algo.init.cherkashinA.TaskCh10N045.findNmember;
import static com.getjavajob.training.algo.init.cherkashinA.TaskCh10N045.sumNmembers;
import static com.getjavajob.training.algo.util.Assert.assertEquals;

public class TaskCh10N045Test {

    public static void main(String[] args) {
        testFindNmember();
        testSumNmembers();
    }

    public static void testFindNmember() {
        assertEquals("TaskCh10N045Test.testFindNmember", 6, findNmember(2, 2, 3));
    }

    public static void testSumNmembers() {
        assertEquals("TaskCh10N045Test.testSumNmembers", 12, sumNmembers(2, 2, 3));
    }

}