/**
 * Created by Александр on 15.03.2017.
 */

package com.getjavajob.training.algo.init.cherkashinA;

public class TaskCh09N017 {

    public static boolean sameFirstNLast(String word) {
        return word.charAt(0) == word.charAt(word.length() - 1);
    }

}
