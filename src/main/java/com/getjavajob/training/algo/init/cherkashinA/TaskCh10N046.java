/**
 * Created by Александр on 17.03.2017.
 */

package com.getjavajob.training.algo.init.cherkashinA;

public class TaskCh10N046 {

    static int findNmember(int firstMember, int denominator, int n) {
        int newN = n;
        newN--;
        if (newN == 0) {
            return firstMember;
        } else {
            return findNmember(firstMember, denominator, newN) * denominator;
        }
    }

    static int sumNmembers(int firstMember, int denominator, int n) {
        int newN = n;
        newN--;
        if (newN == 0) {
            return firstMember;
        } else {
            return sumNmembers(firstMember, denominator, newN) + findNmember(firstMember, denominator, newN) * denominator;
        }
    }

}
