/**
 * Created by Александр on 14.03.2017.
 */

package com.getjavajob.training.algo.init.cherkashinA;

public class TaskCh04N106 {

    public static String countSeason(int month) {
        switch (month) {
            case 12:
            case 1:
            case 2:
                return "Winter";
            case 3:
            case 4:
            case 5:
                return "Spring";
            case 6:
            case 7:
            case 8:
                return "Summer";
            case 9:
            case 10:
            case 11:
                return "Autumn";
            default:
                return "Wrong Month";
        }
    }

}

