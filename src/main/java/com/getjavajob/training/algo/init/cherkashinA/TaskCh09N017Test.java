package com.getjavajob.training.algo.init.cherkashinA;

/**
 * Created by Александр on 09.04.2017.
 */

import static com.getjavajob.training.algo.init.cherkashinA.TaskCh09N017.sameFirstNLast;
import static com.getjavajob.training.algo.util.Assert.assertEquals;

public class TaskCh09N017Test {

    public static void main(String[] args) {
        testSameFirstNLast();
    }

    public static void testSameFirstNLast() {
        assertEquals("TaskCh09N017Test.testSameFirstNLast", true, sameFirstNLast("abracadabra"));
    }

}
