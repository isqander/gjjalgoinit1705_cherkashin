/**
 * Created by Александр on 17.03.2017.
 */

package com.getjavajob.training.algo.init.cherkashinA;

public class TaskCh10N056 {

    static boolean isPrime(int n, int two) {
        if (n == 1) {
            return false;
        }
        if (n / 2 < two) {
            return true;
        }
        if (n % two == 0) {
            return false;
        }
        return isPrime(n, two + 1);
    }

}
