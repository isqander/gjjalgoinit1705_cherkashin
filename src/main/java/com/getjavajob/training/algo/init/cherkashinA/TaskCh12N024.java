/**
 * Created by Александр on 20.03.2017.
 */

package com.getjavajob.training.algo.init.cherkashinA;

import static com.getjavajob.training.algo.init.cherkashinA.TaskCh12N023.printArray;

public class TaskCh12N024 {

    public static void main(String[] args) {

        printArray(makeArrayA(7));
        printArray(makeArrayB(9, 9));

    }

    public static int[][] makeArrayA(int n) {
        int[][] arrayA = new int[n][n];
        for (int i = 0; i < arrayA.length; i++) {
            for (int j = 0; j < arrayA.length; j++) {
                if (i == 0 || j == 0) {
                    arrayA[i][j] = 1;
                }
            }
        }
        for (int i = 1; i < arrayA.length; i++) {
            for (int j = 1; j < arrayA.length; j++) {
                arrayA[i][j] = arrayA[i][j - 1] + arrayA[i - 1][j];
            }
        }
        return arrayA;
    }

    public static int[][] makeArrayB(int n, int m) {
        int[][] arrayB = new int[n][m];
        for (int j = 0; j < arrayB[0].length; j++) {
            arrayB[0][j] = j + 1;
        }
        for (int i = 1; i < arrayB.length; i++) {
            arrayB[i][m - 1] = arrayB[i - 1][0];
            System.arraycopy(arrayB[i - 1], 1, arrayB[i], 0, arrayB[0].length - 1);
        }
        return arrayB;
    }

}
