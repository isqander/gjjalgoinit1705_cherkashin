/**
 * Created by Александр on 15.03.2017.
 */

package com.getjavajob.training.algo.init.cherkashinA;

import java.util.Scanner;

public class TaskCh09N042 {

    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        System.out.println(reverse(scan.nextLine()));
    }

    public static String reverse(String word) {
        StringBuilder buildWord = new StringBuilder(word);
        return buildWord.reverse().toString();
    }

}
