package com.getjavajob.training.algo.init.cherkashinA;

/**
 * Created by Александр on 09.04.2017.
 */

import static com.getjavajob.training.algo.init.cherkashinA.TaskCh09N185.checkParentheses;
import static com.getjavajob.training.algo.util.Assert.assertEquals;

public class TaskCh09N185Test {

    public static void main(String[] args) {
        testCheckParentheses1();
        testCheckParentheses2();
    }

    public static void testCheckParentheses1() {
        assertEquals("TaskCh09N185Test.testCheckParentheses1", "The expression is right", checkParentheses("(...(..)(..)...) "));
    }

    public static void testCheckParentheses2() {
        assertEquals("TaskCh09N185Test.testCheckParentheses2", "#4 left parenthesis is excess", checkParentheses("(..(.(..)(..)...) "));
    }

}

