/**
 * Created by Александр on 12.03.2017.
 */

package com.getjavajob.training.algo.init.cherkashinA;

import static java.lang.Math.*;

public class TaskCh01N017 {

    public static double taskO(double x) {
        return sqrt(1 - pow(sin(x), 2));
    }

    public static double taskP(double x, double a, double b, double c) {
        return 1 / (sqrt(a * pow(x, 2) + b * x + c));
    }

    public static double taskR(double x) {
        return (sqrt(x + 1) + sqrt(x - 1)) / (2 * sqrt(x));
    }

    public static double taskS(double x) {
        return abs(x) + abs(x + 1);
    }

}

