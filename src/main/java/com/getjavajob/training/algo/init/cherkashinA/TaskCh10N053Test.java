package com.getjavajob.training.algo.init.cherkashinA;

/**
 * Created by Александр on 01.05.2017.
 */

import static com.getjavajob.training.algo.init.cherkashinA.TaskCh10N053.reverseAr;
import static com.getjavajob.training.algo.util.Assert.assertEquals;

public class TaskCh10N053Test {

    public static void main(String[] args) {
        testReverseAr();
    }

    public static void testReverseAr() {
        int[] n = {1, 2, 3, 4, 5};
        assertEquals("TaskCh10N053Test.testReverseAr", " 5 4 3 2 1", reverseAr(n));
    }

}
