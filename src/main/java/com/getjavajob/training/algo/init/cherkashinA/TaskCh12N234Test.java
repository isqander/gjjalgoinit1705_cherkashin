package com.getjavajob.training.algo.init.cherkashinA;

/**
 * Created by Александр on 17.04.2017.
 */

import static com.getjavajob.training.algo.init.cherkashinA.TaskCh12N234.*;
import static com.getjavajob.training.algo.util.Assert.assertEquals;

public class TaskCh12N234Test {

    public static void main(String[] args) {
        testDeleteString();
        testDeleteColumn();
    }

    public static void testDeleteString() {
        int[][] outputArray = new int[][]{
                {1, 2, 3, 4, 5, 6},
                {2, 3, 4, 5, 6, 1},
                {3, 4, 5, 6, 1, 2},
                {4, 5, 6, 1, 2, 3},
                {6, 1, 2, 3, 4, 5},
                {0, 0, 0, 0, 0, 0},
        };
        assertEquals("TaskCh12N234Test.testDeleteString", outputArray, deleteString(INPUT_ARRAY, 4));
    }

    public static void testDeleteColumn() {
        int[][] outputArray = new int[][]{
                {1, 2, 3, 4, 6, 0},
                {2, 3, 4, 5, 1, 0},
                {3, 4, 5, 6, 2, 0},
                {4, 5, 6, 1, 3, 0},
                {5, 6, 1, 2, 4, 0},
                {6, 1, 2, 3, 5, 0},
        };
        assertEquals("TaskCh12N234Test.testDeleteColumn", outputArray, deleteColumn(INPUT_ARRAY, 4));
    }

}