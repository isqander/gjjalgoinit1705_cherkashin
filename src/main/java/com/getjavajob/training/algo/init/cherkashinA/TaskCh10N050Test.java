package com.getjavajob.training.algo.init.cherkashinA;

/**
 * Created by Александр on 11.04.2017.
 */

import static com.getjavajob.training.algo.init.cherkashinA.TaskCh10N050.ackerman;
import static com.getjavajob.training.algo.util.Assert.assertEquals;

public class TaskCh10N050Test {

    public static void main(String[] args) {
        testAckerman();
    }

    public static void testAckerman() {
        assertEquals("TaskCh10N050Test.testAckerman", 29, ackerman(3, 2));
    }

}

