/**
 * Created by Александр on 16.03.2017.
 */

package com.getjavajob.training.algo.init.cherkashinA;

public class TaskCh10N043 {

    static int countNumbersA(int a) {
        if (a < 10) {
            return a;
        }
        return countNumbersA(a / 10) + a % 10;
    }

    static int countNumbersB(int a) {
        if (a < 10) {
            return 1;
        }
        return countNumbersB(a / 10) + 1;
    }

}
