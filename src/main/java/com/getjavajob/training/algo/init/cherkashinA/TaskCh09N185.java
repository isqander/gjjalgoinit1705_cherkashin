/**
 * Created by Александр on 16.03.2017.
 */

package com.getjavajob.training.algo.init.cherkashinA;

public class TaskCh09N185 {

    public static String checkParentheses(String expression) {
        char[] expressionArray = expression.toCharArray();
        int leftPar = 0;
        int rightPar = 0;
        for (char ch : expressionArray) {
            if (ch == '(') {
                leftPar++;
            } else if (ch == ')') {
                rightPar++;
            }
            if (leftPar < rightPar) {
                return "Wrong sequence of the parentheses, #" + rightPar + " right parenthesis is excess";
            }
        }
        if (leftPar > rightPar) {
            return "#" + (rightPar + 1) + " left parenthesis is excess";
        } else {
            return "The expression is right";
        }
    }

}
