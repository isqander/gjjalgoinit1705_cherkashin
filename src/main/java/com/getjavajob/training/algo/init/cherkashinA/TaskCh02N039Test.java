package com.getjavajob.training.algo.init.cherkashinA;

/**
 * Created by Александр on 05.04.2017.
 */

import static com.getjavajob.training.algo.init.cherkashinA.TaskCh02N039.findDegree;
import static com.getjavajob.training.algo.util.Assert.assertEquals;

public class TaskCh02N039Test {

    public static void main(String[] args) {
        testFindDegree();
    }

    public static void testFindDegree() {
        assertEquals("TaskCh02N039Test.testFindDegree", 90, findDegree(15, 0, 0));
    }
}

