package com.getjavajob.training.algo.init.cherkashinA;

/**
 * Created by Александр on 07.04.2017.
 */

import static com.getjavajob.training.algo.init.cherkashinA.TaskCh04N015.calculateAge;
import static com.getjavajob.training.algo.util.Assert.assertEquals;

public class TaskCh04N015Test {

    public static void main(String[] args) {
        testCalculateAge1();
        testCalculateAge2();
        testCalculateAge3();
    }

    public static void testCalculateAge1() {
        assertEquals("TaskCh04N015Test.testCalculateAge1", 29, calculateAge(1985, 6, 2014, 12));
    }

    public static void testCalculateAge2() {
        assertEquals("TaskCh04N015Test.testCalculateAge2", 28, calculateAge(1985, 6, 2014, 5));
    }

    public static void testCalculateAge3() {
        assertEquals("TaskCh04N015Test.testCalculateAge3", 29, calculateAge(1985, 6, 2014, 6));
    }

}