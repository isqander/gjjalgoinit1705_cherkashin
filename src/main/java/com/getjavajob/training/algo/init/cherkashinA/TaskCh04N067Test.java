package com.getjavajob.training.algo.init.cherkashinA;

/**
 * Created by Александр on 07.04.2017.
 */

import static com.getjavajob.training.algo.init.cherkashinA.TaskCh04N067.countDay;
import static com.getjavajob.training.algo.util.Assert.assertEquals;

public class TaskCh04N067Test {

    public static void main(String[] args) {
        testCountDay1();
        testCountDay2();
    }

    public static void testCountDay1() {
        assertEquals("TaskCh04N067Test.testCountDay1", "Workday", countDay(5));
    }

    public static void testCountDay2() {
        assertEquals("TaskCh04N067Test.testCountDay2", "Weekend", countDay(7));
    }

}