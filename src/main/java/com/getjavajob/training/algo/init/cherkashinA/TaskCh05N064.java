/**
 * Created by Александр on 14.03.2017.
 */

package com.getjavajob.training.algo.init.cherkashinA;

public class TaskCh05N064 {

    public static double averageDensity(int[] population, double[] square) {
        int totalPopulation = 0;
        double totalSquare = 0;
        for (int i = 0; i < population.length; i++) {
            totalPopulation += population[i];
            totalSquare += square[i];
        }
        return (double) totalPopulation / totalSquare;
    }

}
