/**
 * Created by Александр on 17.03.2017.
 */

package com.getjavajob.training.algo.init.cherkashinA;

import java.util.Arrays;

public class TaskCh10N048 {

    static int maxElement(int[] arr) {
        if (arr.length == 1) {
            return arr[0];
        }
        int[] newArr = arr;
        if (arr[arr.length - 1] > arr[arr.length - 2]) {
            newArr[newArr.length - 2] = arr[arr.length - 1];
        }
        int[] b = Arrays.copyOf(newArr, newArr.length - 1);
        return maxElement(b);
    }

}
