package com.getjavajob.training.algo.init.cherkashinA;

/**
 * Created by Александр on 12.04.2017.
 */

import static com.getjavajob.training.algo.init.cherkashinA.TaskCh10N056.isPrime;
import static com.getjavajob.training.algo.util.Assert.assertEquals;

public class TaskCh10N056Test {

    public static void main(String[] args) {
        testIsPrime1();
        testIsPrime2();
    }

    public static void testIsPrime1() {
        assertEquals("TaskCh10N056Test.testIsPrime1", true, isPrime(7, 2));
    }

    public static void testIsPrime2() {
        assertEquals("TaskCh10N056Test.testIsPrime2", false, isPrime(8, 2));
    }

}
