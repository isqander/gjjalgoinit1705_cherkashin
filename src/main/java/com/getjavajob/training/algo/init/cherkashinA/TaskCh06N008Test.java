package com.getjavajob.training.algo.init.cherkashinA;

/**
 * Created by Александр on 09.04.2017.
 */

import static com.getjavajob.training.algo.init.cherkashinA.TaskCh06N008.notBiggerSquare;
import static com.getjavajob.training.algo.util.Assert.assertEquals;

public class TaskCh06N008Test {

    public static void main(String[] args) {
        testNotBiggerSquare();
    }

    public static void testNotBiggerSquare() {
        assertEquals("TaskCh06N008Test.testNotBiggerSquare", 64, notBiggerSquare(65));
    }

}
