package com.getjavajob.training.algo.init.cherkashinA;

/**
 * Created by Александр on 11.04.2017.
 */

import static com.getjavajob.training.algo.init.cherkashinA.TaskCh10N048.maxElement;
import static com.getjavajob.training.algo.util.Assert.assertEquals;

public class TaskCh10N048Test {

    public static void main(String[] args) {
        testMaxElement();
    }

    public static void testMaxElement() {
        int[] arr = {1, 3, 4, 5, 1};
        assertEquals("TaskCh10N048Test.testMaxElement", 5, maxElement(arr));
    }

}
