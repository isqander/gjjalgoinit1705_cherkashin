/**
 * Created by Александр on 20.03.2017.
 */

package com.getjavajob.training.algo.init.cherkashinA;

import static com.getjavajob.training.algo.init.cherkashinA.TaskCh12N023.printArray;

public class TaskCh12N028 {

    public static void main(String[] args) {
        printArray(makeSpiralArray(9));
    }

    public static int[][] makeSpiralArray(int n) {
        int[][] outputArray = new int[n][n];
        int i = 0;
        for (; i < outputArray.length; i++) {
            if (i < outputArray.length) {
                outputArray[0][i] = i + 1;
            }
        }
        for (int j = 1; j < outputArray.length; j++) {
            outputArray[j][outputArray.length - 1] = i + 1;
            i++;
        }
        for (int j = outputArray.length - 2; j >= 0; j--) {
            outputArray[outputArray.length - 1][j] = i + 1;
            i++;
        }
        for (int j = outputArray.length - 2; j > 0; j--) {
            outputArray[j][0] = i + 1;
            i++;
        }
        String direction = "RIGHT";
        int x = 0;
        int y = 1;
        for (; i < outputArray.length * outputArray.length; i++) {
            switch (direction) {
                case "RIGHT":
                    x++;
                    outputArray[y][x] = i + 1;
                    if (outputArray[y][x + 1] != 0) {
                        direction = "DOWN";
                    }
                    break;
                case "DOWN":
                    y++;
                    outputArray[y][x] = i + 1;
                    if (outputArray[y + 1][x] != 0) {
                        direction = "LEFT";
                    }
                    break;
                case "LEFT":
                    x--;
                    outputArray[y][x] = i + 1;
                    if (outputArray[y][x - 1] != 0) {
                        direction = "UP";
                    }
                    break;
                case "UP":
                    y--;
                    outputArray[y][x] = i + 1;
                    if (outputArray[y - 1][x] != 0) {
                        direction = "RIGHT";
                    }
                    break;
                default:
                    break;
            }
        }
        return outputArray;
    }

}
