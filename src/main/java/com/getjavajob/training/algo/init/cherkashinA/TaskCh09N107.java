/**
 * Created by Александр on 15.03.2017.
 */

package com.getjavajob.training.algo.init.cherkashinA;

public class TaskCh09N107 {

    public static String changeAnO(String wordBuilderd) {
        StringBuilder wordBuilder = new StringBuilder(wordBuilderd);
        int a = wordBuilder.indexOf("a");
        if (a == -1) {
            return "There is no letter \"a\""; //It's better to throw exception, but we can't use it yet
        }
        wordBuilder.reverse();
        int o = wordBuilder.indexOf("o");
        if (o == -1) {
            return "There is no letter \"o\"";
        }
        wordBuilder.replace(o, o + 1, "a");
        wordBuilder.reverse();
        wordBuilder.replace(a, a + 1, "o");
        return wordBuilder.toString();
    }

}
