/**
 * Created by Александр on 17.03.2017.
 */

package com.getjavajob.training.algo.init.cherkashinA;

public class TaskCh10N050 {

    static int ackerman(int n, int m) {
        if (n == 0) {
            return m + 1;
        }
        if (m == 0) {
            return ackerman(n - 1, 1);
        }
        return ackerman(n - 1, ackerman(n, m - 1));
    }

}
