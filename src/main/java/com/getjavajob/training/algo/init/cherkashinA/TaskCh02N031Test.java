package com.getjavajob.training.algo.init.cherkashinA;

/**
 * Created by Александр on 05.04.2017.
 */

import static com.getjavajob.training.algo.init.cherkashinA.TaskCh02N031.findNumber;
import static com.getjavajob.training.algo.util.Assert.assertEquals;

public class TaskCh02N031Test {
    public static void main(String[] args) {
        testFindNumber();
    }

    public static void testFindNumber() {
        assertEquals("TaskCh02N031Test.testFindNumber", 123, findNumber(132));
    }

}
