package com.getjavajob.training.algo.init.cherkashinA;

/**
 * Created by Александр on 13.04.2017.
 */

import static com.getjavajob.training.algo.init.cherkashinA.TaskCh11N245.firstNegative;
import static com.getjavajob.training.algo.util.Assert.assertEquals;

public class TaskCh11N245Test {

    public static void main(String[] args) {
        testFirstNegative();
    }

    public static void testFirstNegative() {
        int[] inputArr = {1, -4, 0, 45, 7, -56, -48};
        int[] outputArr = {-4, -56, -48, 1, 0, 45, 7};
        assertEquals("TaskCh11N245Test.testFirstNegative", outputArr, firstNegative(inputArr));
    }

}
