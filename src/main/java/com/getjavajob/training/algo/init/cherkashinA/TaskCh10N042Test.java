package com.getjavajob.training.algo.init.cherkashinA;

/**
 * Created by Александр on 09.04.2017.
 */

import static com.getjavajob.training.algo.init.cherkashinA.TaskCh10N042.pow;
import static com.getjavajob.training.algo.util.Assert.assertEquals;

public class TaskCh10N042Test {

    public static void main(String[] args) {
        testPow();
    }

    public static void testPow() {
        assertEquals("TaskCh10N042Test.testPow", 8, pow(2, 3));
    }

}
