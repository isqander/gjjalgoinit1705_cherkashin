/**
 * Created by Александр on 17.03.2017.
 */

package com.getjavajob.training.algo.init.cherkashinA;

import java.util.Arrays;

public class TaskCh10N053 {
    static String reversedAr = "";

    public static String reverseAr(int[] n) {
        reversedAr = reversedAr + " " + n[n.length - 1];
        if (n.length == 1) {
            return reversedAr;
        }
        int[] m = Arrays.copyOf(n, n.length - 1);
        reverseAr(m);
        return reversedAr;
    }

    public static void main(String[] args) {
        int[] n = {1, 2, 3, 4, 5};
        System.out.println(reverseAr(n));
    }

}
